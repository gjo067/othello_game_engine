#include "engine.h"
#include "orangemonkey_ai.h"
#include "minimax_ai_depth.h"

#include <thread>
#include <future>
#include <chrono>
#include <functional>
using Clock = std::chrono::high_resolution_clock;

namespace othello
{

bool OthelloGameEngine::initNewGameFromOptions(int playerOneType, int playerTwoType, int playerOneDepth, int playerTwoDepth)
{
    if(playerOneType == 0)
        initPlayerType<HumanPlayer, PlayerId::One>();
    else if(playerOneType == 1)
        initPlayerType<monkey_ais::OrangeMonkeyAI, PlayerId::One>();
    else if(playerOneType == 2)
        initPlayerType<MinimaxAiDepth, PlayerId::One>(playerOneDepth, cornerWeights);

    if(playerTwoType == 0)
        initPlayerType<HumanPlayer, PlayerId::Two>();
    else if(playerTwoType == 1)
        initPlayerType<monkey_ais::OrangeMonkeyAI, PlayerId::Two>();
    else if(playerTwoType == 2)
        initPlayerType<MinimaxAiDepth, PlayerId::Two>(playerTwoDepth, cornerWeights);

    return initNewGame();
}

bool OthelloGameEngine::initNewGame()
{
    clearGame();
    // m_playerId gets set to player two because the start of the game loop switches the current player.
    // So at the start of the game the current player will be player one.
    m_playerId = othello::PlayerId::Two;
    return true;
}

void OthelloGameEngine::clearGame() {
    // clears the bitsets for each player and sets the board to the start positions
    m_board[0].reset();
    m_board[0].set(28);
    m_board[0].set(35);

    m_board[1].reset();
    m_board[1].set(27);
    m_board[1].set(36);
}

bool OthelloGameEngine::performMoveForCurrentHuman(const BitPos& boardPos)
{
    // Check if the space is open and the move is legal.
    if(utility::occupiedPositions(m_board).test(boardPos.value())
            || !utility::isLegalMove(m_board, currentPlayerId(), boardPos))
        return false;

    utility::placeAndFlip(m_board, currentPlayerId(), boardPos);
    return true;
}

void OthelloGameEngine::swapCurrentPlayer() {
    if(m_playerId == PlayerId::One)
        m_playerId = PlayerId::Two;
    else
        m_playerId = PlayerId::One;
}

void OthelloGameEngine::think(const std::chrono::seconds& timeLimit)
{
    BitPos move = BitPos::invalid();
    if(currentPlayerId() == PlayerId::One) {
        m_player_one.obj->think(m_board, currentPlayerId(), timeLimit);
        move = m_player_one.obj->bestMove();
    }
    else {
        m_player_two.obj->think(m_board, currentPlayerId(), timeLimit);
        move = m_player_two.obj->bestMove();
    }
    if(move.isValid())
        utility::placeAndFlip(m_board, currentPlayerId(), move);
}

PlayerId OthelloGameEngine::currentPlayerId() const
{
    return m_playerId;
}

PlayerType OthelloGameEngine::currentPlayerType() const
{
    if(currentPlayerId() == PlayerId::One) {
        return m_player_one.type;
    }
    else {
        return m_player_two.type;
    }
}

BitPieces OthelloGameEngine::pieces(const PlayerId& playerId) const {
    return m_board[size_t(playerId)];
}

const BitBoard& OthelloGameEngine::board() const {
    return m_board;
}


}   // namespace othello
