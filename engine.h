#ifndef ENGINE_H
#define ENGINE_H



#include "utility.h"

#include <basic_types.h>
#include <engine_interface.h>


namespace othello {

class OthelloGameEngine : public othello::GameEngineInterface {
private:
    othello::PlayerId m_playerId = othello::PlayerId::One;

    std::vector<double> customWeights = {
        10, -4, 2, 2, 2, 2, -4, 10,
        -4, -8, 1, 1, 1, 1, -8, -4,
        2, 1, 2, 1, 1, 2, 1, 2,
        2, 1, 1, 1, 1, 1, 1, 2,
        2, 1, 1, 1, 1, 1, 1, 2,
        2, 1, 2, 1, 1, 2, 1, 2,
        -4, -8, 1, 1, 1, 1, -8, -4,
        10, -4, 2, 2, 2, 2, -4, 10
    };
    std::vector<double> cornerWeights = {
        120, -40,  20,  20,  20,  20, -40, 120,
        -40, -60, -10, -10, -10, -10, -80, -40,
        20,  -10,  40,   5,   5,  40, -10,  20,
        20,  -10,   5,   1,   1,   5, -10,  20,
        20,  -10,   5,   1,   1,   5, -10,  20,
        20,  -10,  40,   5,   5,  40, -10,  20,
        -40, -80, -10, -10, -10, -10, -80, -40,
        120, -40,  20,  20,  20,  20, -40, 120
    };
    std::vector<double> sameWeights = {
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1
    };
    std::vector<double> cluneWeights = {
        0.789, 1.379, 1.379, 1.379, 1.379, 1.379, 1.379, 2.015,
        0.592, 0, 0, 0, 0, 0, 0, 0.646,
        0.592, 0, 0, 0, 0, 0, 0, 0.646,
        0.592, 0, 0, 0, 0, 0, 0, 0.646,
        0.592, 0, 0, 0, 0, 0, 0, 0.646,
        0.592, 0, 0, 0, 0, 0, 0, 0.646,
        0.592, 0, 0, 0, 0, 0, 0, 0.646,
        1.221, 1.114, 1.114, 1.114, 1.114, 1.114, 1.114, 3.361
    };

public:
    bool initNewGameFromOptions(int, int, int, int);
    bool initNewGame() override;
    void clearGame() override;
    bool performMoveForCurrentHuman(const othello::BitPos&) override;
    bool legalMovesCheck();
    void swapCurrentPlayer();

    void                think(const std::chrono::seconds&) override;
    othello::PlayerId   currentPlayerId() const override;
    othello::PlayerType currentPlayerType() const override;
    othello::BitPieces  pieces(const othello::PlayerId&) const override;

    const othello::BitBoard& board() const override;
};
} // namespace othello
#endif   // ENGINE_H
