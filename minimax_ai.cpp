#include "minimax_ai.h"

#include "utility.h"
#include <iostream>
#include <sstream>

#include <time.h>
#include <stdlib.h>
#include <chrono>

using Clock = std::chrono::high_resolution_clock;

namespace othello {

MinimaxAi::MinimaxAi(const PlayerId& playerId, std::vector<double> weights)
    : m_weights{weights}, m_depth{0}, m_playerId{playerId}
{
}

void MinimaxAi::think(const BitBoard& board,
                      const PlayerId& playerId,
                      const std::chrono::seconds& maxTime) {

    const auto startTime = Clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - startTime);

    auto root = std::make_shared<BitBoardNode>(board, BitPos{}, playerId);
    root->cost = static_cast<int>(board[size_t(playerId)].count());
    m_bestMove = minimax(root);

    leafNodes.push(root);
    PlayerId currentPlayer = playerId;
    m_depth = 0;

    while(duration.count() < maxTime.count() * 1000 && !leafNodes.empty()) {
        if(leafNodes.front()->playerId != currentPlayer) {
            m_bestMove = minimax(root);
            currentPlayer = utility::opponentId(currentPlayer);
        }

        addNodes(leafNodes.front());
        leafNodes.pop();

        duration = std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - startTime);
    }

    m_bestMove = minimax(root);

    leafNodes = {};
}

BitPos MinimaxAi::bestMove() const {
    const auto best_move = BitPos(m_bestMove.value());
    m_bestMove = BitPos::invalid();
    return best_move;
}

void MinimaxAi::addNodes(std::shared_ptr<BitBoardNode>& parent)
{
    auto legalMoves = utility::legalMoves(parent->board, parent->playerId);
    auto nextPlayerId = utility::opponentId(parent->playerId);

    for(auto move : legalMoves) {
        auto node = std::make_shared<BitBoardNode>(parent->board, move, nextPlayerId);
        utility::placeAndFlip(node->board, node->playerId, move);
        node->cost = calculateCost(node->board);

        parent->children.push_back(node);
        leafNodes.push(node);
    }
}

BitPos MinimaxAi::minimax(const std::shared_ptr<BitBoardNode>& root) {
    BitPos bestMove;
    int bestMoveCost = -1000;
    srand(time(0));

    for(auto child : root->children) {
        int cost = min(child);
        if(cost == bestMoveCost && rand() & 1) {
            bestMove = child->move;
        }
        else if(cost > bestMoveCost) {
            bestMove = child->move;
            bestMoveCost = cost;
        }
    }

    return bestMove;
}

int MinimaxAi::max(const std::shared_ptr<BitBoardNode>& node) {
    if(node->children.empty()) {
        return node->cost;
    }

    int bestMove = -10000;

    for(auto child : node->children)
        bestMove = std::max(min(child), bestMove);

    return bestMove;
}

int MinimaxAi::min(const std::shared_ptr<BitBoardNode>& node) {
    if(node->children.empty()) {
        return node->cost;
    }

    int bestMove = 10000;

    for(auto child : node->children)
        bestMove = std::min(max(child), bestMove);

    return bestMove;
}

int MinimaxAi::calculateCost(const BitBoard& board) {
    int cost = 0;
    size_t playerId = static_cast<size_t>(m_playerId);
    size_t opponentId = m_playerId == PlayerId::One ? 1 : 0;

    for(size_t i = 0; i < detail::computeBoardSize(); ++i) {
        cost += board[playerId][i] * m_weights[63 - i] - board[opponentId][i] * m_weights[63 - i];
    }
    return cost;
}

} // namespace othello

