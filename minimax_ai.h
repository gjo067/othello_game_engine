#ifndef MINIMAX_AI_H
#define MINIMAX_AI_H

#include <player_interface.h>

#include <functional>
#include <queue>

namespace othello {

/*struct BitBoardNode {
    BitBoardNode(BitBoard board, BitPos move, PlayerId playerId)
        : board{board}, move{move}, playerId{playerId} {}
    BitBoardNode(BitBoard board, BitPos move) : board{board}, move{move} {}

    std::vector<std::shared_ptr<BitBoardNode>> children;
    BitBoard board;
    BitPos move;
    PlayerId playerId;
    int cost;
};*/

struct CreateNodesTask {
    std::function<void(std::shared_ptr<BitBoardNode>, const PlayerId&, int)> execute;
};

class MinimaxAi : public AIInterface {

public:
    MinimaxAi(const PlayerId&, std::vector<double> weights);
    void think(const BitBoard& board, const PlayerId& playerId,
               const std::chrono::seconds& maxTime);
    BitPos bestMove() const;

protected:
    void addNodes(std::shared_ptr<BitBoardNode>&);
    BitPos minimax(const std::shared_ptr<BitBoardNode>&);
    int max(const std::shared_ptr<BitBoardNode>&);
    int min(const std::shared_ptr<BitBoardNode>&);
    int calculateCost(const BitBoard&);

    mutable BitPos m_bestMove;
    std::vector<double> m_weights;
    int m_depth;
    PlayerId m_playerId;
    std::queue<std::shared_ptr<BitBoardNode>> leafNodes;
};

}   // namespace othello

#endif // MINIMAX_AI_H
