#include "minimax_ai_depth.h"

#include "utility.h"
#include <iostream>
#include <sstream>

#include <time.h>
#include <stdlib.h>
#include <chrono>
using Clock = std::chrono::high_resolution_clock;

namespace othello {

MinimaxAiDepth::MinimaxAiDepth(const PlayerId& playerId, const int depth,
                     std::vector<double> weights)
    : m_weights{weights}, m_depth{depth}, m_playerId{playerId}
{
}

void MinimaxAiDepth::think(const BitBoard& board,
                      const PlayerId& playerId,
                      const std::chrono::seconds& /*maxTime*/) {
    auto root = std::make_shared<BitBoardNode>(board, BitPos{});
    root->cost = static_cast<int>(board[size_t(playerId)].count());

    createNodes(root, m_playerId, m_depth);

    m_bestMove = minimax(root);
}

BitPos MinimaxAiDepth::bestMove() const {
    const auto best_move = BitPos(m_bestMove.value());
    m_bestMove = BitPos::invalid();
    return best_move;
}

void MinimaxAiDepth::createNodes(std::shared_ptr<BitBoardNode>& parent,
                            const PlayerId& playerId, int depth) {
    --depth;

    auto legalMoves = utility::legalMoves(parent->board, playerId);

    for(auto move : legalMoves) {
        auto node = std::make_shared<BitBoardNode>(parent->board, move);
        utility::placeAndFlip(node->board, playerId, move);
        node-> cost = calculateCost(node->board);
        parent->children.push_back(node);

        if(depth > 0) {
            if(playerId == PlayerId::One)
                createNodes(node, PlayerId::Two, depth);
            else
                createNodes(node, PlayerId::One, depth);
        }
    }
}

BitPos MinimaxAiDepth::minimax(const std::shared_ptr<BitBoardNode>& root) {
    BitPos bestMove;
    int bestMoveCost = -1000;
    srand(time(0));

    for(auto child : root->children) {
        int cost = min(child);
        if(cost == bestMoveCost && rand() & 1) {
            bestMove = child->move;
        }
        else if(cost > bestMoveCost) {
            bestMove = child->move;
            bestMoveCost = cost;
        }
    }

    return bestMove;
}

int MinimaxAiDepth::max(const std::shared_ptr<BitBoardNode>& node) {
    if(node->children.empty()) {
        return node->cost;
    }

    int bestMove = -10000;

    for(auto child : node->children)
        bestMove = std::max(min(child), bestMove);

    return bestMove;
}

int MinimaxAiDepth::min(const std::shared_ptr<BitBoardNode>& node) {
    if(node->children.empty()) {
        return node->cost;
    }

    int bestMove = 10000;

    for(auto child : node->children)
        bestMove = std::min(max(child), bestMove);

    return bestMove;
}

int MinimaxAiDepth::calculateCost(const BitBoard& board) {
    int cost = 0;
    size_t playerId = static_cast<size_t>(m_playerId);
    size_t opponentId = m_playerId == PlayerId::One ? 1 : 0;

    for(size_t i = 0; i < detail::computeBoardSize(); ++i) {
        cost += board[playerId][i] * m_weights[63 - i] - board[opponentId][i] * m_weights[63 - i];
    }
    return cost;
}

} // namespace othello
