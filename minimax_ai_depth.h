#ifndef MINIMAX_AI_DEPTH_H
#define MINIMAX_AI_DEPTH_H

#include <player_interface.h>

namespace othello {

struct BitBoardNode {
    BitBoardNode(BitBoard board, BitPos move) : board{board}, move{move} {}

    std::vector<std::shared_ptr<BitBoardNode>> children;
    BitBoard board;
    BitPos move;
    int cost;
};

class MinimaxAiDepth : public AIInterface
{
public:
    MinimaxAiDepth(const PlayerId&, const int, std::vector<double> weights);
    void think(const BitBoard& board, const PlayerId& playerId,
               const std::chrono::seconds& maxTime);
    BitPos bestMove() const;

protected:
    void createNodes(std::shared_ptr<BitBoardNode>&, const PlayerId&, int);
    BitPos minimax(const std::shared_ptr<BitBoardNode>&);
    int max(const std::shared_ptr<BitBoardNode>&);
    int min(const std::shared_ptr<BitBoardNode>&);
    int calculateCost(const BitBoard&);

    mutable BitPos m_bestMove;
    std::vector<double> m_weights;
    int m_depth;
    PlayerId m_playerId;
};

}   // namespace othello

#endif // MINIMAX_AI_DEPTH_H
