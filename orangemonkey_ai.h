#ifndef ORANGEMONKEY_H
#define ORANGEMONKEY_H

#include <player_interface.h>

#include <random>


namespace othello::monkey_ais
{

class OrangeMonkeyAI : public AIInterface {

    // Constructors
public:
    OrangeMonkeyAI(const PlayerId& player_id);

    // PlayerInterface interface
public:
    void   think(const BitBoard& board, const PlayerId& player_id,
                 const std::chrono::seconds& max_time);
    BitPos bestMove() const;


private:
    mutable BitPos     m_best_move;
    std::random_device m_rd;
    std::mt19937       m_engine;
};

}   // namespace othello::monkey_ais
#endif   // ORANGEMONKEY_H
