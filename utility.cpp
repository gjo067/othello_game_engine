#include "utility.h"

// stl
#include "numeric"
#include "iostream"
#include <vector>

namespace othello::utility
{
////////////////////
//
//
// Interface Utility
// Functions
//
//
////////////////////

BitPieces occupiedPositions(const BitBoard& board/*board*/)
{
    return othello::BitPieces(board[0] | board[1]);
}

bool occupied(const BitPieces& pieces/*pieces*/, const BitPos& boardPos/*board_pos*/)
{
    return pieces[boardPos.value()];
}

bool occupied(const BitBoard& board/*board*/, const BitPos& boardPos/*board_pos*/)
{
    return board[0][boardPos.value()] | board[1][boardPos.value()];
}

BitPos nextPosition(const BitPos& boardPos/*board_pos*/, const MoveDirection& dir/*dir*/)
{
    switch(dir) {
    case othello::MoveDirection::N:
        if(boardPos.value() < 56)
            return BitPos{boardPos.value() + 8};
        break;

    case othello::MoveDirection::NE:
        if(boardPos.value() < 56
                && boardPos.value() % 8 != 0)
            return BitPos{boardPos.value() + 7};
        break;

    case othello::MoveDirection::E:
        if(boardPos.value() % 8 != 0)
            return BitPos{boardPos.value() - 1};
        break;

    case othello::MoveDirection::SE:
        if(boardPos.value() > 7
                && boardPos.value() % 8 != 0)
            return BitPos{boardPos.value() - 9};
        break;

    case othello::MoveDirection::S:
        if(boardPos.value() > 7)
            return BitPos{boardPos.value() - 8};
        break;

    case othello::MoveDirection::SW:
        if(boardPos.value() > 7
                && boardPos.value() % 8 != 7)
            return BitPos{boardPos.value() - 7};
        break;

    case othello::MoveDirection::W:
        if(boardPos.value() % 8 != 7)
            return BitPos{boardPos.value() + 1};
        break;

    case othello::MoveDirection::NW:
        if(boardPos.value() < 56
                && boardPos.value() % 8 != 7)
            return BitPos{boardPos.value() + 9};
        break;
    }
    return BitPos::invalid();
}

BitPos findBracketingPiece(const BitBoard& board,
                           const BitPos& boardPos,
                           const PlayerId& playerId,
                           const MoveDirection& dir)
{
    BitPos nextPos;
    BitPos prevPos(boardPos.value());

    for(int i = 0; i < 8; i++) {
        nextPos = nextPosition(prevPos, dir);

        if(!nextPos.isValid())
            break;

        if(occupied(board, nextPos)) {
            if(occupied(board[size_t(playerId)], nextPos)) {
                if(prevPos == boardPos) break;
                return nextPos;
            }
            else
                prevPos = nextPos;
        }
        else {
            break;
        }
    }

    return BitPos::invalid();
}

BitPieces getPiecesToFlip(const BitBoard& board,
                          const BitPos& boardPos,
                          const PlayerId& playerId,
                          const MoveDirection& dir) {
    BitPieces pieces;

    BitPos nextPos;
    BitPos prevPos {boardPos.value()};

    for(int i = 0; i < 8; i++) {
        nextPos = nextPosition(prevPos, dir);

        if(!nextPos.isValid())
            break;

        if(occupied(board, nextPos)) {
            if(occupied(board[size_t(playerId)], nextPos)) {
                if(prevPos == boardPos) break;
                return pieces;
            }
            else{
                pieces.set(nextPos.value());
                prevPos = nextPos;
            }
        }
        else {
            break;
        }
    }

    return BitPieces{};
}

BitPosSet legalMoves(const BitBoard& board, const PlayerId& playerId)
{
    othello::BitPieces occupiedPositions {othello::utility::occupiedPositions(board)};
    BitPosSet moves {};

    for(size_t i = 0; i < occupiedPositions.size(); i++) {
        if(!occupiedPositions.test(i)) {
            if(isLegalMove(board, playerId, BitPos{i}))
                moves.insert(BitPos{i});
        }
    }

    return moves;
}

bool isLegalMove(const BitBoard& board, const PlayerId& playerId,
                 const BitPos& boardPos)
{
    for(int i = 0; i < 8; i++) {
        if(findBracketingPiece(board, boardPos, playerId, static_cast<othello::MoveDirection>(i)).isValid())
            return true;
    }

    return false;
}


void placeAndFlip(BitBoard& board, const PlayerId& playerId,
                  const BitPos& boardPos)
{
    board[size_t(playerId)][boardPos.value()] = 1;

    BitPieces piecesToFlip{};

    for(int i = 0; i < 8; i++) {
        BitPieces pieces = getPiecesToFlip(board, boardPos, playerId, static_cast<othello::MoveDirection>(i));

        if(pieces.any()){
            piecesToFlip |= pieces;
        }
    }

    if(piecesToFlip.any()) {
        board[0] ^= piecesToFlip;
        board[1] ^= piecesToFlip;
    }
}

PlayerId opponentId(const PlayerId& playerId)
{
    if(playerId == PlayerId::One) return PlayerId{PlayerId::Two};
    else return PlayerId{PlayerId::One};
}

}   // namespace othello::utility
