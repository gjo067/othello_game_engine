#ifndef UTILITY_H
#define UTILITY_H

#include <utility_interface.h>

namespace othello::utility
{
BitPieces getPiecesToFlip(const BitBoard&,
                          const BitPos&,
                          const PlayerId&,
                          const MoveDirection&);

PlayerId opponentId(const PlayerId&);

}   // namespace othello::utility

#endif // UTILITY_H
